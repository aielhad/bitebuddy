**BiteBuddy**

BiteBuddy for the food enthusiasts. Cook, snap, share and start taking orders for your super home-cooked meals. If you love eating, this is your window to a world of flavors. View, choose, order and tip the wonderful cook that made it happen.

BiteBuddy, pop restaurants everywhere!

**The Project**

This is work-in-progress Ruby on Rails application that will later link to Android/iOS apps to serve the Startup's and its customers' goals.