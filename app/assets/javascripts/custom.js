$(function() {
    $('#masonry-container').masonry({
        itemSelector: '.item',
        isFitWidth: true,
        gutterWidth: 10
    });
});