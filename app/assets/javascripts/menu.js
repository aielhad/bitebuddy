// Masonry grid options with infinite scrolling

$(document).on("ready page:load", function() {

    // Fix overlapping item: initialize Masonry first, then trigger layout after images have loaded
    var $container = $('#masonry-container').masonry();

    $container.infinitescroll({
            navSelector: "nav.pagination",
            nextSelector: "nav.pagination a[rel=next]",
            itemSelector: ".item",
            loading: {
                finishedMsg: "You've reached the end of the menu",
                img: 'http://i.imgur.com/6RMhx.gif'
            }
        },

        // trigger Masonry as a callback

        function(newElements) {
            var $newElems = $(newElements);
            $newElems.imagesLoaded(function() {
                $container.masonry('appended', $newElems);
            }); // imagesLoaded
        }
    ); // infinitescroll

    $container.imagesLoaded(function() {
        $container.masonry({
            itemSelector: ".item",
            isFitWidth: true,
            gutter: 10
        });
    }); // imagesLoaded

    // Activating Best In Place
    $(".best_in_place").best_in_place();
});