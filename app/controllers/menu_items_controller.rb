class MenuItemsController < ApplicationController
  before_action :authenticate_user!
  before_action :correct_user, only: [:edit, :update, :destroy] 

  def menu
    # This is a temporary check until working with pagination and infinite scrolling
    @menu_items = MenuItem.sorted_down_by_update_date.page(params[:page])     
  end 

  def show
    @menu_item = MenuItem.find(params[:id])
    respond_to do |format|
        format.html { render 'modal' }
        format.js
    end
  end

  def new
    @menu_item = MenuItem.new
  end

  def create
    @menu_item = current_user.menu_items.build(menu_item_params)
    if @menu_item.save      
      flash[:notice] = "#{@menu_item.title} is added to the menu"
    else
      render 'new'
    end
  end

  def edit
    @menu_item = MenuItem.find(params[:id])
  end

  def update
    @menu_item = MenuItem.find(params[:id])  
    @menu_item.update_attributes(menu_item_params)  
    respond_to do |format|
      if @menu_item.save
        format.html { flash.now[:notice] = "#{@menu_item.title} is updated"; render 'show' }
        format.json { respond_with_bip(@menu_item) }
      else
        format.html { render "edit" }
        format.json { respond_with_bip(@menu_item) }
      end
    end
  end

  def destroy
    @menu_item = MenuItem.find(params[:id])
    @menu_item.destroy
    flash[:notice] = "Menu item deleted"
    redirect_to root_path
  end

  private

    def menu_item_params
      params.require(:menu_item).permit(:title, :description, :expiry_time, :quantity, :same_day, :ingredient_list, :menu_image, { allergen_list: [], payback_list: [] })
    end

    def correct_user
      @menu_item = current_user.menu_items.find_by_id(params[:id])
      redirect_to root_url if @menu_item.nil?
    end
end
