class UsersController < ApplicationController
  before_filter :authenticate_user!, only: [:index, :destroy]
  before_filter :require_admin!, only: [:destroy]

  layout 'show_profile_layout', only: [:show]

  def index
  end

  def show
    @user = User.friendly.find(params[:id])
    @user_menu_items = @user.menu_items.sorted_down_by_update_date
  end

  # def destroy
  #   User.find(params[:id]).destroy
  #   flash[:notice] = "User deleted"
  #   redirect_to root_url    
  # end

  private
  
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
end
