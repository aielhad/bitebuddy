module ApplicationHelper

  # Application title Helper
  def full_title(page_title)
    base_title = "BiteBuddy"  
    if page_title.empty?
     base_title
    elsif :user_signed_in?
      page_title
    else
      "#{page_title} | #{base_title}"
    end
  end

  # Access Devise's resource application-wide
  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  # Rendering error messages
  def error_messages_for(object)
    render 'shared/error_messages', object: object
  end

  def owner?(item)
    item.user == current_user
  end
end

