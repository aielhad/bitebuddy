# t.integer   :user_id
# t.string    :title,         null: false, default: ""
# t.string    :description,   null: false, default: ""
# t.integer   :quantity,      null: false, default: 1
# t.integer   :orders_count,  null: false, default: 0
# t.boolean   :same_day,      null: false, default: false
# t.boolean   :expired,       null: false, default: false
# t.decimal   :minimum_tip,   null: false, default: 5.0, precision: 4, scale: 2
# t.integer   :freshness,     null: false, default: 0
# t.datetime  :expiry_time

class MenuItem < ActiveRecord::Base
  belongs_to :user
  acts_as_taggable_on :ingredients, :allergens, :paybacks
  before_save :title_capitalize # Figure how to pass arguments

  ALLERGENS       = ['Fruit', 'Milk', 'Oats', 'Soy', 'Wheat', 'Eggs', 'Peanuts', 'Tree nut', 
                     'Meat', 'Fish', 'Shellfish', 'Garlic', 'Gluten', 'Sulfites', 'None of the above']
  PAYBACK_METHODS = ['Tipping', 'Chatting', 'A drink', "Any gift but alcohol", 'Just a thank you']

  scope :sorted_down_by_update_date, -> { order('updated_at DESC') }

  # Pagination
  paginates_per 20

  # Paperclip
  has_attached_file :menu_image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/menu_items/:style/missing.png"
  validates_attachment_content_type :menu_image, content_type: /\Aimage\/.*\Z/  
  
  # Validations
  validates :user_id, presence: :true
  validates :title, presence: true,
                    length: { minimum: 5, maximum: 140}
  validates :description, length: { maximum: 255}
  validates :quantity,  presence: :true,
                        numericality: { only_integer: true, greater_than_or_equal_to: 1 }
  validates :same_day, inclusion: [true, false]
  validates :minimum_tip, numericality: { greater_than_or_equal_to: 5.0 }
  validates :freshness, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :expired,  inclusion: [true, false]
  validates :expiry_time, presence: :true,
                          timeliness: { on_or_after: -> { 1.day.from_now }, before: -> { 31.days.from_now }, type: :date }
  validate  :amount_of_ingredients
  validate  :grattitude

  # Capitalize titles with right punctuation.
  def title_capitalize 
  # Move constants to an external text file
  # Change method back to handling aruments
    no_caps = ["a", "an", "and", "abaft", "aboard", "about", "above", "absent", "across", "afore", "after", "against",
               "along", "alongside", "amid", "amidst", "among", "amongst", "an", "anenst", "apropos", "apud", "around",
               "as", "aside", "astride", "at", "athwart", "atop", "barring", "before", "behind", "below", "beneath", "beside",
               "besides", "between", "beyond", "but", "by", "circa", "concerning", "despite", "down", "during", "except", "excluding",
               "failing", "following", "for", "forenenst", "from", "given", "in", "including", "inside", "into", "la", "like", "mid",
               "midst", "minus", "modulo", "near", "next", "notwithstanding", "o", "of", "off", "on", "onto", "opposite", "or",
               "out", "outside", "over", "pace", "past", "per", "plus", "pro", "qua", "regarding", "round", "sans", "save",
               "since", "than", "the", "through", "thru", "throughout", "thruout", "till", "times", "to", "toward", "towards",
               "under", "underneath", "unlike", "until", "unto", "up", "upon", "versus", "vs.", "v.", "via", "vice", "vis-à-vis",
               "with", "within", "without"]
    delimiters = [".", ". ", "?", "? ", "!", "! ", "...", "... "]
    ary = self.attributes['title'].split(/\b/)
    ary.each_with_index do |word, index|
        word.capitalize! if delimiters.include? ary[index-1]
        word.capitalize! unless no_caps.include? word
        word.downcase! if ary[index-1] == "'"
    end
    self.title = ary.join
  end 
  
  private

    def amount_of_ingredients
      number_of_ingredients = ingredient_list.length
      errors.add(:ingredient_list, "should include 2 or more main ingredients") if number_of_ingredients < 2
      errors.add(:ingredient_list, "should not include more than 8 main ingredients") if number_of_ingredients > 8
    end

    def grattitude
      number_of_payback_methods = payback_list.length
      errors.add(:payback_list, "should include at least 2 acceptable forms of grattitude") if number_of_payback_methods < 2
    end
end