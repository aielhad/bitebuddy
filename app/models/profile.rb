# t.integer   :user_id
# t.datetime  :birthday      
# t.string    :phone,         null: false, default: ""
# t.string    :address,       null: false, default: ""
# t.text      :about,         null: false, default: ""
# t.text      :restrictions,  null: false, default: ""  <<-- Use acts-as-taggable on this attribute

class Profile < ActiveRecord::Base
  belongs_to :user

  # Validations
  validates :user_id, presence: true, uniqueness: true
  validates :about, length: { within: 100..750 }, allow_blank: true
  validates :restrictions, length: { maximum: 500}

  # Paperclip
  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/profiles/:style/missing.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/  
end
