# t.string   :username,           null: false, default: ""
# t.string   :email,              null: false, default: ""
# t.string   :encrypted_password, null: false, default: ""
# t.string   :name,               null: false, default: ""
# t.boolean  :admin,              null: false, default: false
# t.boolean  :locked,             null: false, default: false
# t.string   :slug
# t.string   :reset_password_token
# t.datetime :reset_password_sent_at
# t.datetime :remember_created_at
# t.integer  :sign_in_count,      null: false, default: 0
# t.datetime :current_sign_in_at
# t.datetime :last_sign_in_at
# t.string   :current_sign_in_ip
# t.string   :last_sign_in_ip

class User < ActiveRecord::Base
  has_one :profile, dependent: :destroy
  accepts_nested_attributes_for :profile
  before_create :create_profile

  has_many :menu_items, dependent: :destroy
  
  attr_accessor :login

  # Use friendly_id on Users
  extend FriendlyId
  friendly_id :friendify, use: :slugged
  
  # necessary to override friendly_id reserved words
  def friendify
    if username.downcase == "admin"
      "user-#{username}"
    else
      "#{username}"
    end
  end
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
         
  # Pagination
  paginates_per 100
  
  # Validations

  # :name
  validates :name, presence: true
  # :username
  validates :username, uniqueness: { case_sensitive: false }
  validates_format_of :username, with: /\A[a-zA-Z0-9]*\z/, on: :create, message: "can only contain letters and digits"
  validates :username, length: { in: 4..18 }
  before_save { username.downcase! }
  # :email
  validates_format_of :email, with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
  before_save { email.downcase! }
 
  
  def self.paged(page_number)
    order(admin: :desc, username: :asc).page page_number
  end
  
  def self.search_and_order(search, page_number)
    if search
      where("username LIKE ?", "%#{search.downcase}%").order(
      admin: :desc, username: :asc
      ).page page_number
    else
      order(admin: :desc, username: :asc).page page_number
    end
  end
  
  def self.last_signups(count)
    order(created_at: :desc).limit(count).select("id","username","slug","created_at")
  end
  
  def self.last_signins(count)
    order(last_sign_in_at: 
    :desc).limit(count).select("id","username","slug","last_sign_in_at")
  end
  
  def self.users_count
    where("admin = ? AND locked = ?",false,false).count
  end

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      where(conditions).first
    end
  end


  # # Find a way to notify for profile completeness 
  # def profile_complete?
  #   sum = 0
  #   attributes = [self.profile.name, self.profile.birthday, self.profile.restrictions, self.profile.phone, self.profile.address, self.profile.about, self.profile.avatar]
  #   attributes.each do |attribute|
  #     sum += 1 if attribute.blank? # || "default.jpg"
  #   end
  #   sum > 2 ? false : true
  # end
end
