Bitebuddy::Application.routes.draw do

  get "home",    to: "pages#home",      as: "home"
  get "inside",  to: "pages#inside",    as: "inside"
  get "about",   to: "pages#about",     as: "about"
  get "contact", to: "pages#contact",   as: "contact"
  get "terms",   to: "pages#terms",     as: "terms"
    
  devise_for :users, controllers: { sessions: 'users/sessions', registrations: 'users/registrations' }
  resources  :users
  resources  :menu_items

  namespace :admin do
    root "base#index"
    resources :users
  end

  authenticated do
    root to: 'menu_items#menu', as: :authenticated
  end

  root "pages#home"
  
end
