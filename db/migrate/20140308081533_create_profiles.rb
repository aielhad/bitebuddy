class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|

      ## Information about user
      t.integer   :user_id
      t.datetime  :birthday      
      t.string    :phone,         null: false, default: ""
      t.string    :address,       null: false, default: ""
      t.text      :about,         null: false, default: ""
      t.text      :restrictions,  null: false, default: ""

      t.timestamps
    end

    add_index :profiles, :user_id, unique: true
    add_index :profiles, :address
  end
end
