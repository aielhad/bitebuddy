class CreateMenuItems < ActiveRecord::Migration
  def change
    create_table :menu_items do |t|

      t.integer   :user_id
      t.string    :title,         null: false, default: ""
      t.string    :description,   null: false, default: ""
      t.integer   :quantity,      null: false, default: 1
      t.integer   :orders_count,  null: false, default: 0
      t.boolean   :same_day,      null: false, default: false
      t.boolean   :expired,       null: false, default: false
      t.decimal   :minimum_tip,   null: false, default: 5.0, precision: 4, scale: 2
      t.integer   :freshness,     null: false, default: 0
      t.datetime  :expiry_time

      t.timestamps
    end

    add_index :menu_items, :user_id
    add_index :menu_items, :quantity
    add_index :menu_items, :orders_count
    add_index :menu_items, :minimum_tip
    add_index :menu_items, :freshness
    add_index :menu_items, :created_at
    add_index :menu_items, :expiry_time
  end
end
