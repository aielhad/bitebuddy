class AddAttachmentMenuImageToMenuItems < ActiveRecord::Migration
  def self.up
    change_table :menu_items do |t|
      t.attachment :menu_image
    end
  end

  def self.down
    drop_attached_file :menu_items, :menu_image
  end
end
