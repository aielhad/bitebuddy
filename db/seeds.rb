# Temporary admin account
u = User.create(
    name: "Ahmed Elhaddad",
    username: "admin",
    email: "admin@example.com",
    password: "1234",
    password_confirmation: "1234",
    admin: true
)

puts "1 admin account created..."

# Test user accounts
(1..10).each do |i|
  u = User.create(
      name: Faker::Name.name,
      username: "user#{i}",
      email: Faker::Internet.email,
      password: "1234",
      password_confirmation: "1234"
  )
end
puts "10 test users created..."

# Test menu items
User.all.each do |i|
  u = User.find(i)
  10.times do
    item = u.menu_items.new(
        title: Faker::Lorem.sentence(5),
        description: Faker::Lorem.sentence(12),
        quantity: rand(5..15),
        expiry_time: 14.days.from_now
    )
    item.ingredient_list = "Chicken, Foobar, Bazz"
    item.payback_list = "Tipping, Chatting"
    item.save!  
  end 
end

puts "10 test menu items per user created..."