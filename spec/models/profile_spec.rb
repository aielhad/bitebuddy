require 'spec_helper'

describe Profile do
  
  it "should be created automatically with valid user" do
    expect { FactoryGirl.create(:user) }.to change(Profile, :count).by(+1)
  end

  let(:user) { FactoryGirl.create(:user) }
  before { @profile = user.profile }

  subject { @profile }

  it { should respond_to(:user_id) }
  it { should respond_to(:birthday) }
  it { should respond_to(:phone) }
  it { should respond_to(:address) }
  it { should respond_to(:about) }
  it { should respond_to(:restrictions) }
  it { should respond_to(:avatar) }
  its(:user) { should == user }

  it { should be_valid }

  context "when user_id is not present" do
    before { @profile.user_id = nil }
    it { should_not be_valid }
  end

end
