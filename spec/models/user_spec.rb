require 'spec_helper'

describe User do
  
  before do 
    @user = User.new(name: 'Foo Barbaz',
                     username: 'foobar',
                     email: 'foobar@example.com',
                     password: 'password',
                     password_confirmation: 'password'
                     )
  end

  subject { @user }

  it { should be_valid }
  it { should_not be_admin }

  context "with admin rights" do
    before do
      @user.save!
      @user.toggle!(:admin)
    end

    it { should be_admin }
  end

  context "when name is not present" do
    before { @user.name = " " }
    it { should_not be_valid }
  end

  context "when username is not present" do
    before { @user.username = " " }
    it { should_not be_valid }
  end

  context "when username is too short" do
    before { @user.username = "a" * 3 }
    it { should_not be_valid }
  end

  context "when username is too long" do
    before { @user.username = "a" * 19 }
    it { should_not be_valid }
  end

  context "username with mixed case" do
    let(:mixed_case_username) { "FooBAr" }

    it "should be saved as all lower-case" do
      @user.username = mixed_case_username
      @user.save
      @user.reload.username.should == mixed_case_username.downcase
    end
  end

  context "when username format is invalid" do
    it "should be invalid" do
      usernames = %w[fii_bar foo.Bar baz-quzs @ms2l 1235/z *fff23]
      usernames.each do |invalid_username|
        @user.username = invalid_username
        @user.should_not be_valid
      end
    end
  end
  context "when username format is valid" do
    it "should be valid" do
      usernames = %w[user user2 USer11 FOObar 77866]
      usernames.each do |valid_username|
        @user.username = valid_username
        @user.should be_valid
      end
    end
  end

  context "with correct friendly id" do

    before { @user.save }

    context "and not blank" do
      its(:slug) { should_not be_blank }
    end

    its(:slug) { should eq @user.username }
  end


  context "when email is not present" do
    before { @user.email = " " }
    it { should_not be_valid }
  end

  context "when email format is invalid" do
    it "should be invalid" do
      addresses = %w[user@foo,com user_at_foo.org example.user@foo.]
      addresses.each do |invalid_address|
        @user.email = invalid_address
        @user.should_not be_valid
      end
    end
  end

  context "when email format is valid" do
    it "should be valid" do
      addresses = %w[user@foo.COM A_US-ER@f.b.org frst.lst@foo.jp a+b@baz.cn]
      addresses.each do |valid_address|
        @user.email = valid_address
        @user.should be_valid
      end
    end
  end

  context "when email address is already taken" do
    before do
      user_with_same_email = @user.dup
      user_with_same_email.email = @user.email.upcase
      user_with_same_email.save
    end

    it { should_not be_valid }
  end

  context "email address with mixed case" do
    let(:mixed_case_email) { "Foo@ExAMPle.CoM" }

    it "should be saved as all lower-case" do
      @user.email = mixed_case_email
      @user.save
      @user.reload.email.should == mixed_case_email.downcase
    end
  end

  context "when password is not present" do
    before { @user.password = @user.password = " " }
    it { should_not be_valid }
  end

  context "when password confirmation is not present" do
    before { @user.password = @user.password_confirmation = " " }
    it { should_not be_valid }
  end

  context "when password doesn't match confirmation" do
    before { @user.password_confirmation = "mismatch" }
    it { should_not be_valid }
  end

  context "with a password that's too short" do
    before { @user.password = @user.password_confirmation = "a" * 3 }
    it { should_not be_valid }
  end
end

