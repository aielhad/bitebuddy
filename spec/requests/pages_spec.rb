require 'spec_helper'

describe "Pages" do

  subject { page }

  describe "Home page" do
    before { visit root_path }
    
    it { should have_title(full_title('')) }
    it { should_not have_title('Home |') }
    it { should have_selector('h3',  text: 'New to Bitebuddy?') }
    it { should have_selector('p',   text: 'Sign up now') }
    it { should have_submit_button("Sign up") }
    it { should have_submit_button("Sign in") }
    it { should_not have_link('Sign out',  destroy_user_session_path ) }

    context "for signed-in users" do
      before do
        @user ||= FactoryGirl.create :user
        fill_in('Username or email', with: @user.email)
        fill_in('Enter your password', with: @user.password)
        click_button('Sign in')
      end

      it { should have_title(full_title('Menu')) }
      it { should_not have_title('BiteBuddy') }
      it { should have_link 'Sign out', destroy_user_session_path  }
      it { should_not have_submit_button("Sign up") }
      it { should_not have_submit_button("Sign in") }

      context "with admin rights" do
        before do
          @user.toggle!(:admin)
          visit root_path
        end

        it { should have_title(full_title('')) } 
      end
    end
  end

  describe "About page" do
    before { visit about_path }

    it { should have_selector('h1',    text: 'About BiteBuddy') }
    it { should have_title(full_title('About')) }
  end

  describe "Terms page" do
    before { visit terms_path }

    it { should have_selector('h1',    text: 'Terms of Service') }
    it { should have_title(full_title('Terms')) }
  end

  describe "Contact page" do
    before { visit contact_path }

    it { should have_selector('h1',    text: 'Contact') }
    it { should have_title(full_title('Contact')) }
  end

  it "should have the right links on the layout" do
    visit root_path
    click_link "About"
    page.should have_title full_title('About')
    click_link "Terms"
    page.should have_title full_title('Terms')
    click_link "Contact"
    page.should have_title full_title('Contact')
    click_link "Bite Buddy"
    expect(current_path).to eq(root_path)
  end
end
